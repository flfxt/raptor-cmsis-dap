# 猛龙CMSIS-DAP

#### 介绍
猛龙CMSIS-DAP（以下简称“猛龙DAP”）是基于ARM官方的DAPLink v2版本优化而来，更加“好用”，可在嵌入式学习及产品开发过程中代替JLINK进行仿真、调试。

这里主要用于猛龙CMSIS-DAP固件下载。见右侧的“发行版”。

#### 固件升级方法
1. 先将猛龙DAP断电，然后将TX和RX脚短接。
2. 将DAP插到电脑的USB口，此时DAP上TX与RX灯同时亮起（一红一蓝），电脑上会提示U盘插入，并显示名为"BOOTLOADER"的盘符。打开此“U盘”，可见以版本号命名的TXT文件。
3. 将下载的固件解压，得到格式为img的固件文件。将此固件拖放到“U盘”中，等待30秒左右，看到DAP上的TX与RX灯同时熄灭，代表固件升级完成。此时猛龙DAP会自动加载并执行新固件，在KEIL中可以看到升级后的版本号。

 **注意：不要尝试对猛龙DAP固件升级时展示的TXT文件及目录进行其他任何不必要的操作！！！** 

固件升级对版本号没有要求（并非只能升级更高版本），各版本固件可随意烧写。
固件版本号后期并不是越高越好，有些版本可能只是增加或者精简一些功能。猛龙电子会有发布新固件时进行说明。
如果大家有什么建议也可以提出，猛龙电子会在适当的时候做一些有特色的版本方便大家使用。


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx


